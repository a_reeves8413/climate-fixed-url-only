package reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TxtIn {
	
	//any value can be --- and some will have an estimate (*) marker
			//there is provisional after the latest values
			//read the first row of the table to get order of data(?)
			private ArrayList<WeatherData> data;
			private MetaData meta;
			public static final String REGEXONLYNOS = "[^0-9.][^0-9]*";
			public static final String REGEXLETTERSANDNOS = "[^-a-zA-Z0-9.]+";
			//"([\\s]|[^a-zA-Z0-9.])(\\s|[^a-zA-Z0-9])*"
			
			/**
			 * Creates meta data and a list of data of Met office historical information from a file
			 * @param _txtDoc the file location of the document to be read
			 * @throws IOException bad file name or location
			 */
			public TxtIn(String _input) throws IOException{
				
				data = new ArrayList<WeatherData>();
				
				String[] toks = _input.split(REGEXLETTERSANDNOS);
				//printTokens(toks);
				List<String> tokens = new ArrayList<String>();
				for (String tok : toks) {
					if (tok.compareToIgnoreCase("Provisional") == 0);
					else if (tok.compareToIgnoreCase("Site") == 0);
					else if (tok.compareToIgnoreCase("Closed") == 0);
					else tokens.add(parseToken(tok));
				}
				//printTokens(tokens);
				
				//find meta data
				String[] metastuff = new String[5];
				//location name
				metastuff[0] = tokens.remove(0);
				
				while (tokens.remove(0).compareToIgnoreCase("Location") != 0);
				//location data
				metastuff[1] = tokens.remove(0);
				metastuff[2] = tokens.remove(0);
				
				while (tokens.remove(0).compareToIgnoreCase("Lat") != 0);
				metastuff[3] = tokens.remove(0);
				while (tokens.remove(0).compareToIgnoreCase("Lon") != 0);
				metastuff[4] = tokens.remove(0);
				meta = new MetaData(metastuff);
				
				
				//find weather data
				while (tokens.remove(0).compareToIgnoreCase("hours") != 0);
				for (int i=0; i<tokens.size(); i++) {
					data.add(new WeatherData(Integer.parseInt(tokens.get(i++)),Integer.parseInt(tokens.get(i++)),
							Double.parseDouble(tokens.get(i++)), Double.parseDouble(tokens.get(i++)),
							Integer.parseInt(tokens.get(i++)), Double.parseDouble(tokens.get(i++)),
							Double.parseDouble(tokens.get(i))));
				}
				
				
			}

			
			public List<WeatherData> getData() {return data;}
			public MetaData getMeta() {return meta;}
			
			
			/**
			 * Removes common special characters from a string.
			 * It will be used to fix the string so it can be parsed to an integer or double.
			 * @param s
			 * @return the altered String as a
			 */
			private String parseToken(String s) {
				String output = (s.replaceAll("\\*", ""));
				output = output.replaceAll("#", "");
				//output = output.replaceAll("Location", "");
				if (s.contains("---")) return "0";
				else return output;
			}
			
			
			private void printTokens(String[] tokens) {
				for (String token : tokens) System.out.print(token + ", ");
			}
			
			private void printTokens(List<String> tokens) {
				for (String token : tokens) System.out.print(token + ", ");
			}
}
