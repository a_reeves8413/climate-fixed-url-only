package reader;

import org.json.simple.JSONObject;

public class MetaData {

	private final String name;
	private final String locationNorth;
	private final String locationEast;
	private final double latitude;
	private final double longitude;
	
	public MetaData(String _name, String _locE, String _locN, double _lat, double _long) {
		name = _name;
		locationNorth = _locN;
		locationEast = _locE;
		latitude = _lat;
		longitude = _long;
	}
	
	public MetaData(String[] _dataArray) throws ArrayIndexOutOfBoundsException {
			name = _dataArray[0];
			locationEast = _dataArray[1];
			locationNorth = _dataArray[2];
			latitude = Double.parseDouble(_dataArray[3]);
			longitude = Double.parseDouble(_dataArray[4]);
	}
	
	//TODO get methods...

	public JSONObject toJSON() {
		JSONObject output = new JSONObject();
		output.put("name", name);
		output.put("location_East", locationEast);
		output.put("Location_North", locationNorth);
		output.put("latitude", latitude);
		output.put("longitude", longitude);
		return output;
	}
}
