package reader;

import java.util.ArrayList;

public class WeatherDataArray<Object> extends ArrayList{

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder("[");
		for (int i=0; i<size(); i++) {
			output.append(get(i));
			if (i<size()-1) output.append(',');
		}
		output.append(']');
		return output.toString();
	}
}
