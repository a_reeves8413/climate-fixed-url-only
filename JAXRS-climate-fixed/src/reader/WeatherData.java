package reader;

import org.json.simple.JSONObject;

public class WeatherData {

	private final int year, month;
	private final double tMax, tMin;
	private final int airFrost;
	private final double rain;
	private final double sun;
	//any value can be --- and some will have an estimate (*) marker
	
	/**
	 * Is the constructor for the Data object that will store the information for the Json exporter.
	 * @param _year
	 * @param _month
	 * @param _tMax
	 * @param _tMin
	 * @param _af
	 * @param _rain
	 * @param _sun
	 */
	public WeatherData(int _year, int _month, double _tMax, double _tMin, int _af, double _rain, double _sun) {
		year = _year;
		month = _month;
		tMax = _tMax;
		tMin = _tMin;
		airFrost = _af;
		rain = _rain;
		sun = _sun;
	}
	
	
	
	public int getYear() {return year;}
	public int getMonth() {return month;}
	public double getTMax() {return tMax;}
	public double getTMin() {return tMin;}
	public int getAirFrost() {return airFrost;}
	public double getRain() {return rain;}
	public double getSun() {return sun;}
	
	
	
	public JSONObject toJSON() {
		JSONObject output = new JSONObject();
		output.put("year", year);
		output.put("month", month);
		output.put("t_max", tMax);
		output.put("t_min", tMin);
		output.put("air_frost", airFrost);
		output.put("rain", rain);
		output.put("sun", sun);
		return output;
	}
}
