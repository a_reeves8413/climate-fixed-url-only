package com.rest.jersey;

import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.simple.JSONObject;

import reader.TxtIn;

@Path("/json")
public class RestController {

	@GET
	@Path("/first")
	public Response helloWorld() {
		ArrayList<String> output = new ArrayList<String>();
		output.add("Hello, World!");
		return Response.ok().entity(output.get(0))
				.header("Access-Control-Allow-Origin", "*").build();
	}
	
	@GET
	@Path("/data")
	public Response getData(@QueryParam("from") String uri) throws IOException {
		DAO dao = new URLDAO();
		TxtIn ti = dao.getAllDataAndMeta(uri);
		JSONObject jo = DataEncodeService.convertToSingleJSONFromDataAndMeta(ti.getData(), ti.getMeta());
		
		return Response.ok().entity(jo.toJSONString())
				.header("Access-Control-Allow-Origin", "*").build();
	}
	
	@GET
	@Path("/eastbourne")
	public Response eastbourne() throws IOException {
		DAO dao = new URLDAO();
		TxtIn ti = dao.getAllDataAndMeta("eastbourne");
		JSONObject jo = DataEncodeService.convertToSingleJSONFromDataAndMeta(ti.getData(), ti.getMeta());
		
		return Response.ok().entity(jo.toJSONString())
				.header("Access-Control-Allow-Origin", "*").build();
	}
}
