package com.rest.jersey;

import java.util.List;

import org.json.simple.JSONObject;

import reader.MetaData;
import reader.WeatherData;
import reader.WeatherDataArray;

public class DataEncodeService {

	public static JSONObject convertToSingleJSONFromData(List<WeatherData> data) {
		List<Integer> monthVals = new WeatherDataArray<Integer>();
		List<Integer> yearVals = new WeatherDataArray<Integer>();
		List<Integer> afVals = new WeatherDataArray<Integer>();
		List<Double> tMaxVals = new WeatherDataArray<Double>();
		List<Double> tMinVals = new WeatherDataArray<Double>();
		List<Double> rainVals = new WeatherDataArray<Double>();
		List<Double> sunVals = new WeatherDataArray<Double>();
		
		for (WeatherData d : data) {
			monthVals.add(d.getMonth());
			yearVals.add(d.getYear());
			afVals.add(d.getAirFrost());
			tMaxVals.add(d.getTMax());
			tMinVals.add(d.getTMin());
			rainVals.add(d.getRain());
			sunVals.add(d.getSun());
		}
		
		JSONObject output = new JSONObject();
		JSONObject rain = new JSONObject();
			rain.put("values", rainVals);
			rain.put("label", "rainfall (mm)");
			rain.put("unit", "mm");
		JSONObject af = new JSONObject();
			af.put("values", afVals);
			af.put("label", "Air Frost (days)");
			af.put("unit", "days");
		JSONObject tMin = new JSONObject();
			tMin.put("values", tMinVals);
			tMin.put("label", "Temperature low (�C)");
			tMin.put("unit", "�C");
		JSONObject tMax = new JSONObject();
			tMax.put("values", tMaxVals);
			tMax.put("label", "Temperature high (�C)");
			tMax.put("unit", "�C");
		JSONObject sun = new JSONObject();
			sun.put("values", sunVals);
			sun.put("label", "sunlight (hours)");
			sun.put("unit", "hours");
			
		output.put("month", monthVals);
		output.put("year", yearVals);
		output.put("rain", rain);
		output.put("af", af);
		output.put("t_min", tMin);
		output.put("t_max", tMax);
		output.put("sun", sun);
		
		return output;
	}
	
	public static JSONObject convertToSingleJSONFromDataAndMeta(List<WeatherData> data, MetaData meta) {
		JSONObject output= new JSONObject();
		output.put("Meta_data", meta.toJSON());
		output.put("Weather_data", convertToSingleJSONFromData(data));
		return output;
	}
}
