package com.rest.jersey;

import java.io.IOException;
import java.util.List;

import reader.TxtIn;
import reader.WeatherData;

public interface DAO {

	public List<WeatherData> getAllData(String _uri) throws IOException;
	
	public TxtIn getAllDataAndMeta(String _uri) throws IOException;
}
