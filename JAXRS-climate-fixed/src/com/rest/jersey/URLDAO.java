package com.rest.jersey;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import reader.TxtIn;
import reader.WeatherData;

public class URLDAO implements DAO{

	@Override
	public List<WeatherData> getAllData(String _uri) throws IOException {
		URL url = new URL("http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/" +_uri +"data.txt");
		TxtIn ti = new TxtIn(getInput(url));
		return ti.getData();
	}

	@Override
	public TxtIn getAllDataAndMeta(String _uri) throws IOException {
		URL url = new URL("http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/" +_uri +"data.txt");
		TxtIn ti = new TxtIn(getInput(url));
		return ti;
	}

	private String getInput(URL url) throws IOException {
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		return org.apache.commons.io.IOUtils.toString(in, encoding);
	}
}
