package test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.junit.Test;

import com.rest.jersey.DAO;
import com.rest.jersey.DataEncodeService;
import com.rest.jersey.URLDAO;

import reader.TxtIn;

public class TestClass {

	@Test
	public void testURL() throws IOException {
		DAO dao = new URLDAO();
		TxtIn ti = dao.getAllDataAndMeta("sheffield");
		JSONObject jo = DataEncodeService.convertToSingleJSONFromDataAndMeta(ti.getData(), ti.getMeta());
		System.out.print("\n" +jo.toJSONString());
	}

}
